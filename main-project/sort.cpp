#include "sort.h"
#include "phone_conversations.h"
#include <type_traits>
#include <memory>

void heapify(call_record* array[], int size, int root, bool (*compare)(call_record*, call_record*)) {
    int largest = root;
    int left = 2 * root + 1;
    int right = 2 * root + 2;

    if (left < size && compare(array[left], array[largest]))
        largest = left;

    if (right < size&& compare(array[right], array[largest]))
        largest = right;

    if (largest != root) {
        std::swap(array[root], array[largest]);
        heapify(array, size, largest, compare);
    }
}

void heapSort(call_record* array[], int size, bool (*compare)(call_record*, call_record*)) {
    for (int i = size / 2 - 1; i >= 0; i--)
        heapify(array, size, i, compare);

    for (int i = size - 1; i > 0; i--) {
        std::swap(array[0], array[i]);
        heapify(array, i, 0, compare);
    }
}

int partition(call_record* array[], int low, int high, bool (*compare)(call_record*, call_record*)) {
    call_record* pivot = array[high];
    int i = low - 1;

    for (int j = low; j <= high - 1; j++) {
        if (compare(array[j], pivot)) {
            i++;
            std::swap(array[i], array[j]);
        }
    }

    std::swap(array[i + 1], array[high]);
    return i + 1;
}

void quickSort(call_record* array[], int low, int high, bool (*compare)(call_record*, call_record*)) {
    if (low < high) {
        int pi = partition(array, low, high, compare);

        quickSort(array, low, pi - 1, compare);
        quickSort(array, pi + 1, high, compare);
    }
}

bool compareByDurationDesc(call_record* a, call_record* b) {
    if (a->duration.hour != b->duration.hour)
        return a->duration.hour < b->duration.hour;
    if (a->duration.minute != b->duration.minute)
        return a->duration.minute < b->duration.minute;
    return a->duration.second < b->duration.second;
}

bool compareByNumberAscAndCostDesc(call_record* a, call_record* b) {
    int numberComparison = strcmp(a->number, b->number);
    if (numberComparison != 0)
        return numberComparison < 0;

    return a->cost_per_minute > b->cost_per_minute;
}

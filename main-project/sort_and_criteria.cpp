#include "sorting.h"
#include <string.h>

// ������������� ����������
void heapSort(call_record* array[], int size, int (*compare)(const call_record*, const call_record*))
{
    for (int i = size / 2 - 1; i >= 0; i--)
        heapify(array, size, i, compare);

    for (int i = size - 1; i > 0; i--)
    {
        swap(array[0], array[i]);
        heapify(array, i, 0, compare);
    }
}

// ������� ����������
void quickSort(call_record* array[], int low, int high, int (*compare)(const call_record*, const call_record*))
{
    if (low < high)
    {
        int pi = partition(array, low, high, compare);

        quickSort(array, low, pi - 1, compare);
        quickSort(array, pi + 1, high, compare);
    }
}

// �������� ���������
int compareByDurationDesc(const call_record* a, const call_record* b)
{
    return compareTimeDesc(a->duration, b->duration);
}

int compareByCostPerMinuteAsc(const call_record* a, const call_record* b)
{
    if (a->cost_per_minute == b->cost_per_minute)
        return compareStringAsc(a->number, b->number);

    return (a->cost_per_minute < b->cost_per_minute) ? -1 : 1;
}

int compareByNumberAsc(const call_record* a, const call_record* b)
{
    return compareStringAsc(a->number, b->number);
}

// ��������������� �������
void heapify(call_record* array[], int size, int root, int (*compare)(const call_record*, const call_record*))
{
    int largest = root;
    int left = 2 * root + 1;
    int right = 2 * root + 2;

    if (left < size && compare(array[left], array[largest]) > 0)
        largest = left;

    if (right < size && compare(array[right], array[largest]) > 0)
        largest = right;

    if (largest != root)
    {
        swap(array[root], array[largest]);
        heapify(array, size, largest, compare);
    }
}

int partition(call_record* array[], int low, int high, int (*compare)(const call_record*, const call_record*))
{
    call_record* pivot = array[high];
    int i = low - 1;

    for (int j = low; j <= high - 1; j++)
    {
        if (compare(array[j], pivot) < 0)
        {
            i++;
            swap(array[i], array[j]);
        }
    }

    swap(array[i + 1], array[high]);
    return i + 1;
}

int compareTimeDesc(const call_time& a, const call_time& b)
{
    if (a.hour != b.hour)
        return b.hour - a.hour;

    if (a.minute != b.minute)
        return b.minute - a.minute;

    return b.second - a.second;
}

int compareStringAsc(const char* a, const char* b)
{
    return strcmp(a, b);
}

void swap(call_record*& a, call_record*& b)
{
    call_record* temp = a;
    a = b;
    b = temp;
}
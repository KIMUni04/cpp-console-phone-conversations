#ifndef CALL_RECORD_H
#define CALL_RECORD_H

#include "constants.h"

struct call_date
{
    int day;
    int month;
    int year;
};

struct call_time
{
    int hour;
    int minute;
    int second;
};

struct call_record
{
    char number[MAX_STRING_SIZE];
    call_date date;
    call_time time;
    call_time duration;
    char tariff[MAX_STRING_SIZE];
    double cost_per_minute;
};

#endif
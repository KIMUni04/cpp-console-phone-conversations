#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

call_date convertDate(char* str)
{
    call_date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.day = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.month = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.year = atoi(str_number);
    return result;
}

call_time convertTime(char* str)
{
    call_time result;
    char* context = NULL;
    char* str_number = strtok_s(str, ":", &context);
    result.hour = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.minute = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.second = atoi(str_number);
    return result;
}

void readCallRecords(const char* file_name, call_record* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            call_record* item = new call_record;
            file >> item->number;
            file >> tmp_buffer;
            item->date = convertDate(tmp_buffer);
            file >> tmp_buffer;
            item->time = convertTime(tmp_buffer);
            file >> tmp_buffer;
            item->duration = convertTime(tmp_buffer);
            file >> item->tariff;
            file >> item->cost_per_minute;

            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}

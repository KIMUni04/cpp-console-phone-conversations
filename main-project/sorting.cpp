#include "sorting.h"

void heapSort(call_record* array[], int size, int (*compare)(const call_record*, const call_record*)) {
    for (int i = size / 2 - 1; i >= 0; i--)
        heapify(array, size, i, compare);

    for (int i = size - 1; i > 0; i--) {
        swap(array[0], array[i]);
        heapify(array, i, 0, compare);
    }
}

void quickSort(call_record* array[], int low, int high, int (*compare)(const call_record*, const call_record*)) {
    if (low < high) {
        int pi = partition(array, low, high, compare);

        quickSort(array, low, pi - 1, compare);
        quickSort(array, pi + 1, high, compare);
    }
}

int (*selectHeapSort())(const call_record*, const call_record*) {
    return compareByDurationDesc;
}

int (*selectQuickSort())(const call_record*, const call_record*) {
    return compareByNumberAsc;
}

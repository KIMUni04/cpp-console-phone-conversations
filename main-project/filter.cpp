#include "filter.h"
#include "phone_conversations.h"
#include <cstring>
#include <iostream>

call_record** filter(call_record* array[], int size, bool (*check)(call_record* element), int& result_size)
{
	call_record** result = new call_record * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_call_record_by_mobile_tariff(call_record* element)
{
	return strcmp(element->tariff, "мобильный") == 0;
}

bool check_call_record_by_date(call_record* element)
{
	return element->date.month == 11 && element->date.year == 21;
}

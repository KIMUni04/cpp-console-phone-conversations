#include <iostream>
#include <iomanip>
#include "phone_conversations.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
#include "sort.h"

using namespace std;

void output(call_record* record)
{
    cout << "�����.............: " << record->number << '\n';
    cout << "���� ���������....: ";
    cout << setw(2) << setfill('0') << record->date.day << '.';
    cout << setw(2) << setfill('0') << record->date.month << '.';
    cout << setw(2) << setfill('0') << record->date.year << '\n';
    cout << "����� ������......: ";
    cout << setw(2) << setfill('0') << record->time.hour << ':';
    cout << setw(2) << setfill('0') << record->time.minute << ':';
    cout << setw(2) << setfill('0') << record->time.second << '\n';
    cout << "�����������������.: ";
    cout << setw(2) << setfill('0') << record->duration.hour << ':';
    cout << setw(2) << setfill('0') << record->duration.minute << ':';
    cout << setw(2) << setfill('0') << record->duration.second << '\n';
    cout << "�����.............: " << record->tariff << '\n';
    cout << "���������.........: " << record->cost_per_minute << '\n';
    cout << '\n';
}

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �9. ���������� ���������\n";
    cout << "�����: ����� ��������\n";
    cout << "������: 23����1�\n\n";

    call_record* callRecords[MAX_FILE_ROWS_COUNT];
    int size;

    try
    {
        readCallRecords("data.txt", callRecords, size);

        cout << "***** ���������� ��������� *****\n\n";

        for (int i = 0; i < size; i++)
        {
            output(callRecords[i]);
        }

        bool (*check_function)(call_record*) = nullptr;

        cout << "\n�������� ��������:\n";
        cout << "1. ������� ��� ���������� ��������� �� ������ '���������'\n";
        cout << "2. ������� ��� ���������� ��������� �� ������ 2021 ����.\n";
        cout << "3. ������������� ������\n";  // ����� ����� ��� ����������
        int item;

        cin >> item;
        cout << '\n';

        switch (item)
        {
        case 1:
            check_function = check_call_record_by_mobile_tariff;
            cout << "***** ���������� ��������� �� ������ '���������' *****\n\n";
            break;
        case 2:
            check_function = check_call_record_by_date;
            cout << "***** ���������� ��������� �� ������ 2021 ���� *****\n\n";
            break;
        case 3:
            int sortMethod, compareMethod;

            cout << "�������� ����� ����������:\n";
            cout << "1. ������������� ���������� (Heap sort)\n";
            cout << "2. ������� ���������� (Quick sort)\n";
            cin >> sortMethod;

            bool (*compareFunction)(call_record*, call_record*) = nullptr;

            cout << "�������� �������� ���������:\n";
            cout << "1. �� �������� ����������������� ���������\n";
            cout << "2. �� ����������� ������ ��������, � � ������ ������ ������ �� �������� ��������� ���������\n";
            cin >> compareMethod;

            switch (compareMethod) {
            case 1:
                compareFunction = compareByDurationDesc;
                break;
            case 2:
                compareFunction = compareByNumberAscAndCostDesc;
                break;
            default:
                cout << "������������ ����� �������� ���������.\n";
                return 1;
            }

            switch (sortMethod) {
            case 1:
                heapSort(callRecords, size, compareFunction);
                break;
            case 2:
                quickSort(callRecords, 0, size - 1, compareFunction);
                break;
            default:
                cout << "������������ ����� ������ ����������.\n";
                return 1;
            }

            cout << "***** ��������������� ���������� ��������� *****\n\n";
            for (int i = 0; i < size; i++) {
                output(callRecords[i]);
            }
            return 0;
        }

        if (check_function)
        {
            int new_size;

            call_record** filtered = filter(callRecords, size, check_function, new_size);

            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }

            delete[] filtered;
        }

        for (int i = 0; i < size; i++)
        {
            delete callRecords[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }

    return 0;
}

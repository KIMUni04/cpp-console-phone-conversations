#ifndef FILE_READER_H
#define FILE_READER_H

#include "phone_conversations.h"

void readCallRecords(const char* file_name, call_record* array[], int& size);

#endif

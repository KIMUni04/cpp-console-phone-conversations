#ifndef SORTING_H
#define SORTING_H

#include "phone_conversations.h"

void heapify(call_record* array[], int size, int root, int (*compare)(const call_record*, const call_record*));
void swap(call_record*& a, call_record*& b);
int partition(call_record* array[], int low, int high, int (*compare)(const call_record*, const call_record*));
int compareTimeDesc(const call_time& a, const call_time& b);
int compareStringAsc(const char* a, const char* b);


void heapSort(call_record* array[], int size, int (*compare)(const call_record*, const call_record*));
void quickSort(call_record* array[], int low, int high, int (*compare)(const call_record*, const call_record*));

int compareByDurationDesc(const call_record* a, const call_record* b);
int compareByCostPerMinuteAsc(const call_record* a, const call_record* b);
int compareByNumberAsc(const call_record* a, const call_record* b);

int (*selectHeapSort())(const call_record*, const call_record*);
int (*selectQuickSort())(const call_record*, const call_record*);

void sortRecords(call_record* array[], int size, int (*compare)(const call_record*, const call_record*)) {
    heapSort(array, size, compare);
}
#endif
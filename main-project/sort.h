#ifndef SORTING_H
#define SORTING_H

#include "phone_conversations.h"

void heapify(call_record* array[], int size, int root, bool (*compare)(call_record*, call_record*));

void heapSort(call_record* array[], int size, bool (*compare)(call_record*, call_record*));

int partition(call_record* array[], int low, int high, bool (*compare)(call_record*, call_record*));

void quickSort(call_record* array[], int low, int high, bool (*compare)(call_record*, call_record*));

bool compareByDurationDesc(call_record* a, call_record* b);

bool compareByNumberAscAndCostDesc(call_record* a, call_record* b);
#endif

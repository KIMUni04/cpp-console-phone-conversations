#ifndef FILTER_H
#define FILTER_H

#include "phone_conversations.h"

call_record** filter(call_record* array[], int size, bool (*check)(call_record* element), int& result_size);

bool check_call_record_by_mobile_tariff(call_record* element);

bool check_call_record_by_date(call_record* element);

#endif